#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

## If running from tty1 start sway
#if [ "$(tty)" = "/dev/tty1" ]; then
# 	exec sway
# fi

HISTCONTROL=ignoreboth
shopt -s histappend
HISTSIZE=50000
HISTFILESIZE=200000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS
shopt -s checkwinsize 


alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi


if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/.cargo/bin" ] ; then
    PATH="$HOME/.cargo/bin:$PATH"
fi


alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'


_ps1bash() {
	exitcode="$?"
	r="\[\e[0m\]"
	bg_red="\[\e[41m\]"
	bg_yellow="\[\e[43m\]"
	bg_blue="\[\e[44m\]"
	fg_black="\[\e[30m\]"
	fg_red="\[\e[31m\]"
	fg_green="\[\e[32m\]"
	fg_yellow="\[\e[33m\]"
	fg_blue="\[\e[34m\]"
	fg_magenta="\[\e[35m\]"
	fg_grey="\[\e[37m\]"
	fg_bold="\[\e[1m\]"

	# Add hostname prefix in SSH sessions
	unset prefix
	# Alert if in an SSH session
	if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
		prefix="${fg_bold}${fg_red}$(hostname -s)${r}${fg_red}:${r}${prefix}"
	fi
	# Change PWD color depending on the shell
	case $0 in
		*bash)
			prefix="${prefix}${fg_blue}"
			;;
		*ksh)
			prefix="${prefix}${fg_green}"
			;;
		*)
			;;
	esac
	# Show the tilde instead of $HOME
	if [ "${PWD#$HOME}" != "$PWD" ]; then
		cpwd="~${PWD#$HOME}"
	else
		cpwd="$PWD"
	fi
	# Show read-only status in certain directories
	if [ "$UID" = 0 ]; then
		# Fuck you I'm root
		prompt="${fg_red}#${r}"
	elif ! [ -d "$PWD" ]; then
		# What the fuck happened to my directory
		prompt="${bg_red}${fg_black}!${r}"
	elif ! [ -r "$PWD" ]; then
		# Can't read is worst case
		prompt="${fg_red}"'$'"${r}"
	elif ! [ -x "$PWD" ]; then
		# Can read it but can't search it is weird but also bad
		if [ -w "$PWD" ]; then
			# Fixable
			prompt="${bg_blue}${fg_black}"'$'"${r}"
		else
			# Unfixable
			prompt="${bg_yellow}${fg_black}"'$'"${r}"
		fi
	elif ! [ -w "$PWD" ]; then
		# Can't write is really common but also good to know
		prompt="${fg_magenta}"'~'"${r}"
	else
		# Otherwise we should be fine
		prompt="${fg_green}"'$'"${r}"
	fi
	# Alert us if the last command failed
	unset fail
	if ! [ "$exitcode" = "0" ]; then
		fail="${fg_bold}${fg_red}?"
	fi
	# shellcheck disable=2059
	PS1="[${prefix}${cpwd}${r}]${fail}${r}${prompt}${r} "
}


export PS1=""
export PROMPT_COMMAND=_ps1bash



# Aliases
alias :q="exit"
