#!/bin/sh

screenshot() {
case $1 in
  partial)
  maim -s | tee ~/Pictures/Screenshots/$(date +%Y-%m-%d_%H:%M:%S).png | xclip -selection clipboard -t image/png
  ;;
  window)
  sleep 1
  maim -u -i $(xdotool selectwindow) | tee ~/Pictures/Screenshots/$(date +%Y-%m-%d_%H:%M:%S).png | xclip -selection clipboard -t image/png
    ;;
  *)
    ;;
  esac;
}

screenshot $1
